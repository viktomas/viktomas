# Tomas Vik

I'm a maintainer of 
- [GitLab Workflow VS Code Extension](https://gitlab.com/gitlab-org/gitlab-vscode-extension)
- [GitLab Language Server](https://gitlab.com/gitlab-org/editor-extensions/gitlab-lsp)
- [GitLab `glab` CLI tool](https://gitlab.com/gitlab-org/cli)
- [GitLab WebIDE](https://gitlab.com/gitlab-org/gitlab-web-ide)
- [GitLab fork of VS Code](https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/)

 I'm also a frontend reviewer and contributor to the [GitLab project](https://gitlab.com/gitlab-org/gitlab).

I like writing:

- GitLab
  - [VS Code extension development with GitLab](https://about.gitlab.com/blog/2020/11/30/vscode-extension-development-with-gitlab/)
  - [How to do GitLab merge request reviews in VS Code](https://about.gitlab.com/blog/2021/01/25/mr-reviews-with-vs-code/)
  - [Visual Studio code editor: Eight tips for using GitLab VS Code](https://about.gitlab.com/blog/2021/05/20/vscode-workflows-for-working-with-gitlab/)
  - [How to write a GitLab blog post](https://gitlab.com/-/snippets/2357996)
- [blog.viktomas.com](https://blog.viktomas.com)

## Personal projects

- [godu](https://github.com/viktomas/godu): Simple golang utility helping to discover large files/folders.
- [Simpleton Analytics](https://gitlab.com/viktomas/simpleton-analytics): Simpleton is privacy-first, basic web analytics.
- [slipbox](https://gitlab.com/viktomas/slipbox): Zettelkasten support for VS Code
- [Total YouTube Watchtime](https://gitlab.com/viktomas/total-youtube-watchtime): find out how much of your life did you spend watching YouTube
- [canonical](https://gitlab.com/viktomas/canonical): CLI tool that helps you find duplicate files
